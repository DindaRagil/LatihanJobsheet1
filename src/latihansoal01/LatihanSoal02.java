
package latihansoal01;


public class LatihanSoal02 {
  public static void cetak() {
        System.out.println("SMK Telkom Malang");
    }

    public static void cetak(int i) {
        if (i > 0) {
            System.out.println("SMK Telkom Malang");
            cetak(--i);
        }
    }

    public static void main(String[] args) {
        String identitas = "Dinda Ragil Pramuda Wardani/X RPL6/21";
        System.out.println("Identitas : " + identitas);
        cetak(5);
    }  
}
