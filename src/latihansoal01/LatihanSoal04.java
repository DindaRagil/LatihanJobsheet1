
package latihansoal01;

import java.util.Scanner;

public class LatihanSoal04 {
   public static void cetakAngka(int angka) {
        if (angka >= 1) {
            System.out.print(angka + " ");
            cetakAngka(--angka);
        }

    }

    public static void main(String[] args) {
        Scanner baca = new Scanner(System.in);
        String identitas = "Dinda Ragil Pramuda Wardani/X RPL6/21";
        System.out.println("Identitas: " + identitas);
        System.out.print("Masukkan sembarang angka: ");
        int N = baca.nextInt();

        cetakAngka(N);
    } 
}
