
package latihansoal01;

import java.util.Scanner;

public class LatihanSoal05 {

    public static int faktorial(int r) {
        if (r == 0 || r == 1) {
            return 1;
        } else {
            return r * faktorial(r - 1);
        }

    }

    public static void main(String[] args) {
        String identitas = "Dinda Ragil Pramuda Wardani/X RPL6/21";
        System.out.println("Identitas: " + identitas);
        System.out.println("Menghitung Faktorial dengan Rekursif");
        Scanner scan = new Scanner(System.in);
        System.out.print("Masukkan bilangan: ");
        int bil = scan.nextInt();
        int hasil = faktorial(bil);
        System.out.println("Hasil: " + hasil);
    }
}
